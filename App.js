import React, {useState} from 'react';
import { ScrollView } from 'react-native';
import { TextInput } from 'react-native';
import { StyleSheet, Text, View, Button, Image, Linking } from 'react-native';
import  Contact  from './Contact';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import DetailsScreen from './Details';
import Main from './Main';

const Stack = createStackNavigator();

export default function App() {
  
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Main">
        {props => <Main {...props}/>}
        </Stack.Screen>
        <Stack.Screen name="Details">
        {props => <DetailsScreen {...props}/>}
        </Stack.Screen>
      </Stack.Navigator>
    </NavigationContainer>
  )
}
