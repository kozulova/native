import React from 'react';
import { StyleSheet, Text, View, Image, TouchableHighlight, Button } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

export default function App({ contact, navigation }) {
  return (
    <View style={styles.container}>
      <Image
        style={styles.tinyLogo}
        source={{
          uri: 'https://cdn.pixabay.com/photo/2017/06/13/12/53/profile-2398782_1280.png',
        }}
      />
      <Text style={{marginRight: 20}}>{contact.name}</Text>
      <Text>{contact.email}</Text>
     {/*} <TouchableHighlight onPress={onPress} activeOpacity={0.6} underlayColor="#DDDDDD"></TouchableHighlight> */}
        <Button
          style={styles.info}
          title="I"
          onPress={() => navigation.navigate('Details', contact)}
        />
      </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: '#e7e9f4',
    padding: 30,
    alignItems: 'center',
    borderWidth: 2,
    borderColor: "#20232a",
    borderRadius: 3,
  },
  tinyLogo: {
    width: 50,
    height: 50,
    marginRight: 20
  },
  info: {
    borderWidth: 2,
    borderColor: "#20232a",
    borderRadius: 30,
    height: 30,
    width: 30,
    textAlign: 'center',
    paddingTop: 5,
    marginLeft: 10
  }
});