import React from 'react';
import { StyleSheet, Text, View, Button, Image, Linking } from 'react-native';


export default function DetailsScreen({navigation, route}) {
    const {name, email, phone} = route.params; 
    return (
      <View style={{ flex: 1}}>
        <Button title="Go back" onPress={() => navigation.goBack()} />
        <View style={{alignItems: 'center', justifyContent: 'center', marginTop: 10, marginBottom: 10}}>
        <Image
          style={styles.tinyLogo}
          source={{
            uri: 'https://cdn.pixabay.com/photo/2017/06/13/12/53/profile-2398782_1280.png',
          }}
        />        
        <Text>{name}</Text>
        <Text>{phone}</Text>
        </View>
        <Button onPress={()=>{Linking.openURL(`tel:${phone}`);}} title="Call"/>
        <Button onPress={()=>{console.log("DELETE")}} title="Delete"/>
      </View>
    );
  }
 
  const styles = StyleSheet.create({
    tinyLogo: {
      width: 100,
      height: 100,
      marginRight: 20
    },
  });  