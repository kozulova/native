import React, {useState} from 'react';
import { ScrollView } from 'react-native';
import { TextInput } from 'react-native';
import { StyleSheet, Text, View, Button, Image, Linking } from 'react-native';
import  Contact  from './Contact';

const contacts = Array.from({length: 10}, (v, i) => {
    return {
      "name": "Jon Doe",
      "email": "email@gmail.com",
      "phone": "+38097456773"
    }});

export default function Main({navigation}) {
    const [text, setText] = useState('');
    return (
      <View style={styles.container}>
       <TextInput
          placeholder="Enter name"
          value={text}
          style={styles.input}
          onChange={(e)=>setText(e.target.value)}
        />
          <Text>Contacts </Text>
          <ScrollView>
          {contacts.filter(c=>c.name.toLocaleLowerCase().indexOf(text)!=-1).map((c, i) => <Contact key={i} contact={c} navigation={navigation}></Contact>)}
          </ScrollView>
      </View>
    );
  }
  
  const styles = StyleSheet.create({
    container: {
      //flex: 1,
      backgroundColor: '#fff',
      padding: 20,
      justifyContent: 'center',
    },
    input: {
      height: 40,
      margin: 12,
      borderWidth: 1,
    },
    tinyLogo: {
      width: 100,
      height: 100,
      marginRight: 20
    },
  });